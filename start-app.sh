
docker service rm springboot-project

sleep 5

sudo rm -rf project
git clone https://gitlab.com/SpringBoot-POC/spring-session-redis.git project

docker service create \
  --name springboot-project \
  --mode replicated --replicas 2 --endpoint-mode vip \
  --network my-subnet1 \
  --mount type=bind,src="/home/hung/docker/spring-boot/project",dst="/project" \
  --workdir "/project" \
  kwonghung/docker-git-gradle \
  gradle bootrun
