
docker service rm core-portainer
docker secret rm portainer-admin-passwd 

docker service rm core-nginx

docker rm -f core-redis

docker network rm my-subnet1

sleep 5

docker network create \
  --driver overlay \
  --attachable \
  --subnet 204.11.22.1/26 \
  my-subnet1

echo password1234 | docker secret create portainer-admin-passwd - 

docker service create \
  --name core-portainer \
  --mode global --endpoint-mode dnsrr \
  --network my-subnet1 \
  --publish published=9000,target=9000,mode=host \
  --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
  --mount type=volume,src=portainer-data,dst=/data \
  --secret source=portainer-admin-passwd,target=/tmp/admin_password \
  portainer/portainer 
#  \ --admin-password-file /tmp/admin_password

docker service create \
  --name core-nginx \
  --mode global \
  --network my-subnet1 \
  --publish published=80,target=80,mode=host \
  --publish published=442,target=442,mode=host \
  nginx:1.13.12-alpine

docker run -it -d \
  --name core-redis \
  --network my-subnet1 \
  --restart always \
  redis:3.2.11-alpine

#docker service create \

#docker run -it --rm \
#  kwonghung/docker-git-gradle bash

