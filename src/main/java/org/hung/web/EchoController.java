package org.hung.web;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hung.CounterRecord;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class EchoController {
	
	@GetMapping("/greeting")
	public String greeting() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return "Hello! " + auth;
	}

	@GetMapping("/counter")
	public ModelAndView sessionCounter(HttpSession session) {
		CounterRecord record = new CounterRecord();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String login = auth.getName();
		record.setLogin(login);
		
		String hostname;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			hostname = e.getMessage();
		}
		record.setHostname(hostname);
		
		Integer counter = (Integer)session.getAttribute("counter");
		if (counter==null) {
			counter = 1;
		}
		record.setCounter(counter++);
		session.setAttribute("counter", counter);
		
		record.setTimestamp(new Date());
		
		List<CounterRecord> list = (List<CounterRecord>)session.getAttribute("list");
		if (list==null) {
			list = new ArrayList<CounterRecord>();
		}
		list.add(record);
		if (list.size()>50) {
			list.remove(0);
		}
		session.setAttribute("list", list);
		
		ModelAndView result = new ModelAndView("counter");
		result.addObject("sessionId",session.getId());
		result.addObject("list",list);
		return result;
	}
}
